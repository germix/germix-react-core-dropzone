import React from 'react';

interface Props
{
    disabled?,
    files: File[],
    onDrop(files),
    onRemove(file),
    multiple? : boolean,
    acceptedMimeTypes? : string[],
}
interface State
{
    dragging: boolean,
}

/**
 * @author Germán Martínez
 */
class DropZone extends React.Component<Props,State>
{
    state: State = 
    {
        dragging: false,
    }
    dropRef;
    dragCounter = 0;

    render()
    {
        return (
<div
    className={'drop-zone' + ((this.state.dragging) ? ' dragging' : '')}
    ref={(e) => this.dropRef = e}
    onClick={() =>
    {
        if(this.props.disabled)
        {
            return;
        }
        if(this.isAllowed(null))
        {
            let input = document.createElement("input");
            let onFocus = (e) =>
            {
                setTimeout(() =>
                {
                    document.body.removeChild(input);
                },
                2000);
                document.body.removeEventListener('focus', onFocus);
            }
            input.type = 'file';
            if(this.props.acceptedMimeTypes)
                input.accept = this.props.acceptedMimeTypes.join(',');
            input.style.display = 'none';
            input.addEventListener('change', (e) =>
            {
                this.props.onDrop(input.files);
            });
            document.body.addEventListener('focus', onFocus);
            document.body.appendChild(input);
            input.click();
        }
    }}
>
    {this.state.dragging &&
    <div className="drop-zone-overlay">
    </div>
    }
    <div className="drop-zone-content">
        { this.props.files.map((file: File, index) =>
        {
            return (
                <div key={index} className="drop-zone-file">
                    { file.name }
                    <div
                        className="drop-zone-file-x"
                        onClick={(e) =>
                        {
                            e.preventDefault();
                            e.stopPropagation();
                            if(this.props.disabled)
                            {
                                return;
                            }
                            this.props.onRemove(file);
                        }}
                    >x
                    </div>
                </div>
            );
        })}
    </div>
</div>
        );
    }

    isAllowed = (e : DragEvent|null) =>
    {
        if(this.props.files.length > 0 && !this.props.multiple)
        {
            return false;
        }
        if(e && e.dataTransfer && e.dataTransfer.items != null && this.props.acceptedMimeTypes)
        {
            let valid = false;
            for(let i = 0; i < e.dataTransfer.items.length && !valid; i++)
            {
                const item = e.dataTransfer.items[i];
                if(item.kind == 'file')
                {
                    for(let j = 0; j < this.props.acceptedMimeTypes.length && !valid; j++)
                    {
                        if(item.type == this.props.acceptedMimeTypes[j])
                        {
                            valid = true;
                        }
                    }
                }
            }
            return valid;
        }
        return true;
    }

    isForbidden = (e : DragEvent) =>
    {
        return !this.isAllowed(e);
    }

    handleDragOver = (e) =>
    {
        e.preventDefault();
        e.stopPropagation();
        if(this.isForbidden(e))
        {
            e.dataTransfer.dropEffect = 'none';
            e.dataTransfer.effectAllowed = 'none';
        }
    }

    handleDragEnter = (e: DragEvent) =>
    {
        e.preventDefault();
        e.stopPropagation();
        this.dragCounter++;
        if(e.dataTransfer && e.dataTransfer.items && e.dataTransfer.items.length > 0)
        {
            if(this.isForbidden(e))
            {
                e.dataTransfer.dropEffect = 'none';
                e.dataTransfer.effectAllowed = 'none';
            }
            else
            {
                this.setState({
                    dragging: true,
                });
            }
        }
    }

    handleDragLeave = (e) =>
    {
        e.preventDefault();
        e.stopPropagation();
        this.dragCounter--;
        if(this.dragCounter > 0)
        {
            return;
        }
        this.setState({
            dragging: false,
        });
    }

    handleDrop = (e) =>
    {
        e.preventDefault();
        e.stopPropagation();

        if(e.dataTransfer.files && e.dataTransfer.files.length > 0)
        {
            this.props.onDrop(e.dataTransfer.files);
            e.dataTransfer.clearData();
            this.dragCounter = 0;
        }
        this.setState({
            dragging: false,
        });
    }

    componentDidMount()
    {
        this.dropRef.addEventListener('dragenter', this.handleDragEnter);
        this.dropRef.addEventListener('dragleave', this.handleDragLeave);
        this.dropRef.addEventListener('dragover', this.handleDragOver);
        this.dropRef.addEventListener('drop', this.handleDrop);
    }

    componentWillUnmount()
    {
        this.dropRef.addEventListener('dragenter', this.handleDragEnter);
        this.dropRef.addEventListener('dragleave', this.handleDragLeave);
        this.dropRef.addEventListener('dragover', this.handleDragOver);
        this.dropRef.addEventListener('drop', this.handleDrop);
    }
}
export default DropZone;
