# Germix React Core - DropZone

## About

Germix react core dropzone components

## Installation

```bash
npm install @germix/germix-react-core-dropzone
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
